--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:02:58 08/04/2022
-- Design Name:   
-- Module Name:   D:/Ejercicios_U10/TB_Sumador4_Bits.vhd
-- Project Name:  Ejercicios_U10
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Sumador4_Bits
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_Sumador4_Bits IS
END TB_Sumador4_Bits;
 
ARCHITECTURE behavior OF TB_Sumador4_Bits IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Sumador4_Bits
    PORT(
         A : IN  std_logic_vector(3 downto 0);
         B : IN  std_logic_vector(3 downto 0);
         Cin : IN  std_logic;
         Cout : OUT  std_logic;
         S : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal Cin : std_logic := '0';

 	--Outputs
   signal Cout : std_logic;
   signal S : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Sumador4_Bits PORT MAP (
          A => A,
          B => B,
          Cin => Cin,
          Cout => Cout,
          S => S
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
-- hold reset state for 100 ns.
      wait for 100 ns;	
		A <= "0001";
		B <= "0001";
		
		wait for 100 ns;	
		A <= "0010";
		B <= "0011";
		
		wait for 100 ns;	
		A <= "0100";
		B <= "0101";
		
		wait for 100 ns;	
		A <= "0110";
		B <= "0101";
		
		wait for 100 ns;	
		A <= "1000";
		B <= "0111";
      -- insert stimulus here 
      wait;
   end process;

END;
