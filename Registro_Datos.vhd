----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:20:06 08/04/2022 
-- Design Name: 
-- Module Name:    Registro_Datos - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Registro_Datos is
port (
   B: inout std_logic;
   RO: inout std_logic_vector (3 downto 0);
   RD: inout std_logic_vector (3 downto 0));
end Registro_Datos;

architecture Behavioral of Registro_Datos is
begin
   process (B, RD, RO) begin
     if (B'event and B='1') then
        RD <= RO;
     end if;
   end process;
end Behavioral;

