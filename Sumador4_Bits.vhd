----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:50:44 08/04/2022 
-- Design Name: 
-- Module Name:    Sumador4_Bits - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Sumador4_Bits is
port(
		 A : in STD_LOGIC_VECTOR(3 downto 0);
		 B : in STD_LOGIC_VECTOR(3 downto 0);  
		 Cin: in Std_logic;
		 Cout : out STD_LOGIC;
		 S : out STD_LOGIC_VECTOR(3 downto 0));
end Sumador4_Bits;

architecture Behavioral of Sumador4_Bits is
component Sumador_Completo is
	 port(
		 A : in STD_LOGIC;
		 B : in STD_LOGIC;
		 Ci : in STD_LOGIC;
		 Suma : out STD_LOGIC;
		 Cout : out STD_LOGIC
	     );
end component;	   
signal c: std_logic_vector(3 downto 0);
begin
	
	Sum0: Sumador_Completo port map(A(0), B(0), Cin, S(0), C(0));	  
	Sum1: Sumador_Completo port map(A(1), B(1), C(0), S(1), C(1));
	Sum2: Sumador_Completo port map(A(2), B(2), C(1), S(2), C(2));
	Sum3: Sumador_Completo port map(A(3), B(3), C(2), S(3), Cout);
end Behavioral;

