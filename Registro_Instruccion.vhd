----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:21:17 08/04/2022 
-- Design Name: 
-- Module Name:    Registro_Instruccion - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Registro_Instruccion is
port  (
    A: inout std_logic;
    RO: inout std_logic_vector (3 downto 0) ;
    RI: inout std_logic_vector (3 downto 0));
end Registro_Instruccion;

architecture Behavioral of Registro_Instruccion is
begin
   process (A, RO, RI) begin
    if (A'event and A= '1') then
        RI <= RO;
     end if;
   end process;
end Behavioral;

