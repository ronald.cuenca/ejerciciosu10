----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:18:44 08/04/2022 
-- Design Name: 
-- Module Name:    Deco_Instruccion - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Deco_Instruccion is
port(
    RI: inout std_logic_vector  (3 downto 0);
    DI: inout std_logic_vector  (0 to 7));
end Deco_Instruccion;

architecture Behavioral of Deco_Instruccion is
begin
   process (RI) begin
        case RI is
          when "0000" => DI <= "10000000"; -- funci�n and
          when "0001" => DI <= "01000000"; -- funci�n or
          when "0010" => DI <= "00100000"; -- funci�n xor
          when "0011" => DI <= "00010000"; -- funci�n suma aritm�tica
          when "0100" => DI <= "00001000"; -- funci�n NOT
          when "0101" => DI <= "00000100"; -- funci�n ret�n
          when "0110" => DI <= "00000010"; -- funci�n carga
          when "0111" => DI <= "00000001"; -- funci�n reset
          when others => DI <= "00000000"; -- se inhabilita el DI
      end case;
   end process;
end Behavioral;

