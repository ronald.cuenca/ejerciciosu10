----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:11:31 08/04/2022 
-- Design Name: 
-- Module Name:    Registro_Acumulador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Registro_Acumulador is
Port ( C : inout  STD_LOGIC;
           OP : inout  STD_LOGIC_VECTOR (3 downto 0);
           ACT : inout  STD_LOGIC_VECTOR (3 downto 0));
end Registro_Acumulador;

architecture Behavioral of Registro_Acumulador is
begin
process (C, ACT, OP) begin
 
 if (C'event and C = '1') then
			ACT <= OP;	
end if;

end process;
end Behavioral;

