----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:10:44 08/04/2022 
-- Design Name: 
-- Module Name:    ListadoALU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ListadoALU is
Port ( DI : inout  STD_LOGIC_VECTOR (0 to 7);
           RD : inout  STD_LOGIC_VECTOR (3 downto 0);
           ACC : inout  STD_LOGIC_VECTOR (3 downto 0);
           OP : inout  STD_LOGIC_VECTOR (3 downto 0));
end ListadoALU;

architecture Behavioral of ListadoALU is
begin

	process (DI, ACC, RD) begin
	if (DI = "10000000") then
	OP <= ACC and RD;
	elsif (DI = "01000000")then
	OP <= ACC or RD;
	elsif (DI = "00100000")then
	OP <= ACC xor RD;
	elsif (DI = "00001000")then
	OP <= not ACC;
	elsif (DI = "00000100")then
	OP <=  ACC; --HOLD
	else
	OP <= RD; --CARGAR ACUMULADOR
	end if;
	end process;
end Behavioral;

