----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:59:27 08/04/2022 
-- Design Name: 
-- Module Name:    Sumador_Completo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Sumador_Completo is
port(
		 A : in STD_LOGIC;
		 B : in STD_LOGIC;
		 Ci : in STD_LOGIC;
		 Suma : out STD_LOGIC;
		 Cout : out STD_LOGIC);
end Sumador_Completo;

architecture Behavioral of Sumador_Completo is
begin
	Suma <= A xor B xor Ci;
	Cout <= (A and B) or (A and Ci) or (B and Ci);
end Behavioral;

