----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:13:09 08/04/2022 
-- Design Name: 
-- Module Name:    Contador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Contador is
    Port ( E : inout  STD_LOGIC;
           DI : inout  STD_LOGIC_VECTOR (0 to 7);
           PC : inout  STD_LOGIC_VECTOR (3 downto 0));
end Contador;

architecture Behavioral of Contador is
begin
	process (E) begin
	if (E'event and E = '1') then
		PC <= PC + 1;
		if (DI = "00000001") then
		PC <=  "0000";
		end if;
	end if;
	end process;
end Behavioral;

