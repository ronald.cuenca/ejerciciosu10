----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:01:32 08/04/2022 
-- Design Name: 
-- Module Name:    Multiplicador2_Bits - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Multiplicador2_Bits is
Port ( A, B : in  STD_LOGIC_VECTOR (1 downto 0);
           S : out  STD_LOGIC_VECTOR (2 downto 0);
           Cout : out  STD_LOGIC);
end Multiplicador2_Bits;

architecture Behavioral of Multiplicador2_Bits is
	Signal P : STD_LOGIC_VECTOR (3 downto 0);
	Signal C : STD_LOGIC;
begin
	
	P(0) <= B(0) AND A(0);
	P(1) <= B(0) AND A(1);
	P(2) <= B(1) AND A(0);
	P(3) <= B(1) AND A(1);
	
	S(0) <= P(0);
	S(1) <= (P(1) XOR P(2)) XOR '0';
	C <= (P(1) AND P(2)) OR ((P(1) XOR P(2)) AND '0');
	S(2) <= ('0' XOR P(3)) XOR C;
	Cout <= ('0' AND P(3)) OR (('0' XOR P(2)) AND C);
end Behavioral;

